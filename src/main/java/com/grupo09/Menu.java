package com.grupo09;

import java.io.File;
import java.util.Scanner;


public class Menu {

    private int YEARS = 1;

    public boolean mainMenu(boolean firstRun, File file, String runningPath) {
        int option = 2;
        if(file != null) {
            option = 1;
        }

        String populationName = "";
        double[][] matrix;
        double[] vector;
        double[] s;
        double[] f;

        switch (option) {
            case 1:
                matrix = Utils.buildMatrix(file);
                vector = Utils.buildVector(file);
                secondaryMenu(firstRun, matrix, vector, file, runningPath);
                break;
            case 2:
                vector = new double[0];
                s = new double[0];
                f = new double[0];
                insertMenu(populationName, vector, s, f, runningPath);
                break;
            default:
                return false;
        }
        return false;
    }

    public void insertMenu(String populationName, double[] vector, double[] s, double[] f, String runningPath) {
        Scanner in = new Scanner(System.in);

        System.out.println();
        System.out.println("Pressione o número da opção pretendida");
        System.out.println();
        System.out.println("Nota: Inserir valores separados por vírgula!");
        System.out.println();
        System.out.println("1. Inserir nome da população");
        System.out.println("2. Inserir valores do vector");
        System.out.println("3. Inserir valores da taxa de sobrevivência");
        System.out.println("4. Inserir valores do número médio de indivíduos reprodutores");
        System.out.println();
        System.out.println("4. Voltar menú anterior");
        System.out.println("0. Sair");
        System.out.print("opção> ");

        int option = in.nextInt();

        switch (option) {
            case 1:
                Utils.clearScreen();
                populationName = setPopulationName(populationName);
                checkNextStep(vector, s, f, runningPath);
                insertMenu(populationName, vector, s, f, runningPath);
                break;
            case 2:
                Utils.clearScreen();
                vector = generateVector(vector);
                checkNextStep(vector, s, f, runningPath);
                insertMenu(populationName, vector, s, f, runningPath);
                break;
            case 3:
                Utils.clearScreen();
                s = generateVector(s);
                checkNextStep(vector, s, f, runningPath);
                insertMenu(populationName, vector, s, f, runningPath);
                break;
            case 4:
                Utils.clearScreen();
                f = generateVector(f);
                checkNextStep(vector, s, f, runningPath);
                insertMenu(populationName, vector, s, f, runningPath);
                break;
            case 0:
                System.exit(0);
            default:
                Utils.clearScreen();
                insertMenu(populationName, vector, s, f, runningPath);
                break;
        }
    }

    public boolean secondaryMenu(boolean firstRun, double[][] matrix, double[] vector, File inputFile, String runningPath) {
        Scanner in = new Scanner(System.in);

        if(firstRun) {
            Utils.clearScreen();
            System.out.println("Dados carregados correctamente");
            System.out.println();
            System.out.println("Inserir o número de anos que a simulação correr");
            System.out.print("anos> ");
            YEARS = 1 + in.nextInt();
        }
        System.out.println();
        System.out.println("Pressione o número da opção pretendida\n");
        System.out.println("1. Imprimir matriz de Leslie");
        System.out.println("2. Calcular o número total de indivíduos por ano");
        System.out.println("3. Calcular o crescimento anual da população");
        System.out.println("4. Calcular a distribuição da população por ano");
        System.out.println("5. Calcular a distribuição da população por ano (normalizada)");
        System.out.println("6. Calcular maior valor próprio e vetor associado");
        System.out.println("8. Limpar valores e começar de novo");
        System.out.println("0. Sair");
        System.out.print("opção> ");

        int option = in.nextInt();
        String directory = "";

        String populationName = inputFile.getName().substring(0, inputFile.getName().indexOf('.'));
        double[][] populationMatrix = Algebra.totalPopulationPerClassPerYear(YEARS, matrix, vector);
        double[] totalPopulationPerYear = Algebra.totalPopulationPerYear(YEARS, populationMatrix);
        String[] classNames = Utils.getClassNames(inputFile);



        switch (option) {
            case 1:
                Utils.clearScreen();
                Utils.printLeslieMatrix(matrix);
                secondaryMenu(false, matrix, vector, inputFile, runningPath);
                break;
            case 2:
                Utils.clearScreen();
                System.out.println("Número total de indivíduos");
                Utils.printVector(totalPopulationPerYear, YEARS, "(t, Nt)");
                Utils.writeVectorToFile(totalPopulationPerYear,  "temp/temp.txt");
                Graph.execGraph("Anos" , "Total População", new String[]{"Total População"});
                directory = MenuHelper.chooseDirectoryMenu(runningPath);
                if(!directory.equals("")) {
                    String[] extension = MenuHelper.choseFormatMenu();
                    if(extension.length > 0) {
                        Graph.saveGraph(populationName, "Anos", "Total População", new String[]{"Total População"}, extension, "TotalPopulacao");
                    }
                }
                secondaryMenu(false, matrix, vector, inputFile, runningPath);
                break;
            case 3:
                Utils.clearScreen();
                double[] populationGrowth = Algebra.populationGrowth(YEARS, populationMatrix);
                System.out.println("Crescimento da população");
                Utils.printVector(populationGrowth, YEARS - 1, "(t, delta_t)");
                Utils.writeVectorToFile(populationGrowth,  "temp/temp.txt");
                Graph.execGraph("Anos" , "Crescimento População", new String[]{"Total População"});
                directory = MenuHelper.chooseDirectoryMenu(runningPath);
                if(!directory.equals("")) {
                    String[] extension = MenuHelper.choseFormatMenu();
                    if(extension.length > 0) {
                        Graph.saveGraph(populationName, "Anos", "Crescimento da População", new String[]{"Crescimento População"}, extension, "CrescimentoPopulacao");
                    }
                }
                secondaryMenu(false, matrix, vector, inputFile, runningPath);
                break;
            case 4:
                Utils.clearScreen();
                double[][] notNormalized = Algebra.totalPopulationPerClassPerYear(YEARS, matrix, vector);
                String headerNonNormalized = Utils.buildHeader(matrix);
                System.out.println("Número por classe (não normalizado)");
                Utils.printMatrix(notNormalized, headerNonNormalized);

                Utils.writeMatrixToFile(notNormalized,  "temp/temp.txt");
                Graph.execGraph("Anos" , "Classes não normalizadas", classNames);
                if(!directory.equals("")) {
                    String[] extension = MenuHelper.choseFormatMenu();
                    if(extension.length > 0) {
                        Graph.saveGraph(populationName, "Anos", "Classes não normalizadas", classNames, extension, "ClassesNaoNormalizadas");
                    }
                }
                secondaryMenu(false, matrix, vector, inputFile, runningPath);
                break;
            case 5:
                Utils.clearScreen();
                double[][] normalized = Algebra.populationPerClassNormalized(populationMatrix, totalPopulationPerYear);
                String headerNormalized = Utils.buildHeader(matrix);
                System.out.println("Número por classe (não normalizado)");
                Utils.printMatrix(normalized, headerNormalized);

                Utils.writeMatrixToFile(normalized,  "temp/temp.txt");
                Graph.execGraph("Anos" , "Classes não normalizadas", classNames);
                if(!directory.equals("")) {
                    String[] extension = MenuHelper.choseFormatMenu();
                    if(extension.length > 0) {
                        Graph.saveGraph(populationName, "Anos", "Classes normalizadas", classNames, extension, "ClassesNormalizadas");
                    }
                }
                secondaryMenu(false, matrix, vector, inputFile, runningPath);
                break;
            case 6:
                Utils.clearScreen();
                double lambda = Algebra.calcMaxEigenValue(matrix);
                double[] eigenVector = Algebra.calcMaxEigenVector(matrix);
                System.out.println("Maior valor proprio e vetor associado");
                System.out.println("lambda: " + lambda);
                Utils.printEigenVector(eigenVector);
                secondaryMenu(false, matrix, vector, inputFile, runningPath);
                break;
            case 8:
                Utils.clearScreen();
                mainMenu(true, inputFile, runningPath);
                break;
            case 0:
                System.exit(0);
        }
        return false;
    }

    /** method
     * @code receive vector, survival rate and average of reproductive individuals
     *
     * @implNote  check if matrix is empty and pass to another menu
     *
     * @return
     */
    private void checkNextStep(double[] vector, double[] s, double[] f, String workDir) {
        double[][] matrix;
        boolean go;
        matrix = buildManualMatrix(vector, s, f);
        if (matrix.length > 0) {
            go = checkFinalMatrix(matrix);
            if (go) secondaryMenu(true, matrix, vector, null, workDir);
        }
    }

    /** method check matrix
     *
     * @code matrix input
     * @implNote print the matrix and check it
     *
     *
     * @return the answer
     */
    private boolean checkFinalMatrix(double[][] auxMatrix) {
        Scanner in = new Scanner(System.in);
        String answer;
        System.out.println("A matriz está como é pretendido? [sim(default) ou não]");
        Utils.printLeslieMatrix(auxMatrix);
        answer = in.nextLine().toLowerCase();
        return answer.equals("") || answer.equals("sim");
    }

    private double[][] buildManualMatrix(double[] vector, double[] s, double[] f) {
        double[][] matrix = new double[0][0];
        if (vector.length > 0 && s.length > 0 && f.length > 0) {
            matrix = Utils.buildManualMatrix(f, s);
        }
        return matrix;
    }

    private double[] generateVector(double[] vector) {
        if (vector.length <= 0) {
            Scanner input = new Scanner(System.in);
            String line = input.nextLine();
            vector = Utils.insertVector(line);
        }
        return vector;
    }

    private String setPopulationName(String populatationName) {
        if(populatationName.equals("")) {
            Scanner input = new Scanner(System.in);
            populatationName = input.nextLine();
        }
        return populatationName;
    }
}


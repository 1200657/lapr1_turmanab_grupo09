package com.grupo09;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class MenuHelper {

    /**
     * Method to leave
     * {@code File}. File information, menu?
     *
     * @implNote
     *
     * @return end the program
     */
    public static void interactive(Menu menu, File file, String runningPath) {
        boolean quit;
        do {
            Utils.createTempFolder("tmp/");
            quit = menu.mainMenu(true, file, runningPath);
        }
        while (quit);
    }

    /**
     * Method to no Interative
     * {@code File}. Input from terminal..
     *
     * @implNotes call all math methods. ?
     *
     * @return output txt
     */
    public static void nonInteractive(String[] args) {
        double[][] matrix;
        double[] vector;
        String[] extension = new String[0];
        String[] classNames;
        int years = -1;
        int fileFormat = 0;
        boolean e = false;
        boolean v = false;
        boolean r = false;
        String[] cmd;
        String populationName;

        for(int i = 0; i < args.length; i++) {
            if(args[i].equals("-t")) {
                years = Integer.parseInt(args[i + 1]) + 1;
                if(years < 0 || years > 200) {
                    System.out.println("Número de gerações inserido não pode ser menor que 0 e maior que 200");
                    return;
                }
            }
            if(args[i].equals("-g")) {
                fileFormat = Integer.parseInt(args[i+1]);
                switch (fileFormat) {
                    case 1:
                        extension = new String[]{"txt", "txt"};
                        break;
                    case 2:
                        extension = new String[]{"pngcairo", "png"};
                        break;
                    case 3:
                        extension = new String[]{"eps", "eps"};
                        break;
                    default:
                        System.out.println("Formato escolhido não válido, selecione: \n 1 para png \n 2 para txt \n 3 para eps");
                        return;
                }
            }
            if(args[i].equals("-e")) {
                e = true;
            }
            if(args[i].equals("-v")) {
                v = true;
            }
            if(args[i].equals("-r")) {
                r = true;
            }
        }

        File inputFile = new File(args[args.length - 2]);
        if(inputFile.exists() && !inputFile.isDirectory()) {
            Utils.createTempFolder("temp/");
            populationName = inputFile.getName().substring(0, inputFile.getName().indexOf('.'));
            classNames = Utils.getClassNames(inputFile);
            matrix = Utils.buildMatrix(inputFile);
            vector = Utils.buildVector(inputFile);

            try {
                File outputFile = new File(args[args.length - 1]);
                FileWriter fileWriter = new FileWriter(outputFile);
                PrintWriter printWriter = new PrintWriter(fileWriter);

                // Número de Gerações
                printWriter.println("K=" + (years - 1));
                printWriter.println();

                // Matriz de Leslie
                printWriter.println("Matriz de Leslie");
                Utils.formatLeslieMatrixToFile(printWriter, matrix);
                printWriter.println();

                double[][] populationMatrix = Algebra.totalPopulationPerClassPerYear(years, matrix, vector);
                double[] totalPopulationPerYear = Algebra.totalPopulationPerYear(years, populationMatrix);

                // Número total de individuos
                if(v) {
                    printWriter.println("Número total de indivíduos");
                    Utils.formatVectorToFile(printWriter, totalPopulationPerYear, years, "(t, Nt)");
                    Utils.writeVectorToFile(totalPopulationPerYear,  "temp/temp.txt");
                    //Graph.execGraph("Anos" , "Total População", new String[]{"Total População"});
                    Graph.saveGraph(populationName, "Anos", "Total População", new String[]{"Total População"}, extension, "TotalPopulacao");
                    printWriter.println();
                }

                // Crescimento da população
                if(r) {
                    double[] populationGrowth = Algebra.populationGrowth(years, populationMatrix);
                    printWriter.println("Crescimento da população");
                    Utils.formatVectorToFile(printWriter, populationGrowth, years - 1, "(t, delta_t)");

                    Utils.writeVectorToFile(populationGrowth,  "temp/temp.txt");
                    //Graph.execGraph("Anos" , "Crescimento População", new String[]{"Total População"});
                    Graph.saveGraph(populationName, "Anos", "Crescimento da População", new String[]{"Crescimento População"}, extension, "CrescimentoPopulacao");
                    printWriter.println();
                }

                //Número de Classes não normalizado
                double[][] notNormalized = Algebra.totalPopulationPerClassPerYear(years, matrix, vector);
                String headerNonNormalized = Utils.buildHeader(matrix);
                printWriter.println("Número por classe (não normalizado)");
                Utils.formatMatrixToFile(printWriter,notNormalized, headerNonNormalized);

                Utils.writeMatrixToFile(notNormalized,  "temp/temp.txt");
                //Graph.execGraph("Anos" , "Classes não normalizadas", classNames);
                Graph.saveGraph(populationName, "Anos", "Classes não normalizadas", classNames, extension, "ClassesNaoNormalizadas");

                printWriter.println();

                //Número de Classes normalizado
                double[][] normalized = Algebra.populationPerClassNormalized(populationMatrix, totalPopulationPerYear);
                String headerNormalized = Utils.buildHeader(matrix);
                printWriter.println("Número por classe (normalizado)");
                Utils.formatMatrixToFile(printWriter,normalized, headerNormalized);

                Utils.writeMatrixToFile(normalized,  "temp/temp.txt");
                //Graph.execGraph("Anos" , "Classes normalizadas", classNames);
                Graph.saveGraph(populationName, "Anos", "Classes normalizadas", classNames, extension, "ClassesNormalizadas");

                printWriter.println();

                //Maior valor próprio e vetor associado
                if(e) {
                    double lambda = Algebra.calcMaxEigenValue(matrix);
                    double[] eigenVector = Algebra.calcMaxEigenVector(matrix);
                    printWriter.println("Maior valor proprio e vetor associado");
                    printWriter.println("lambda: " + lambda);
                    Utils.formatEigenVectorToFile(printWriter, eigenVector);
                }

                printWriter.close();
            } catch (IOException ex) {
                System.out.println("Erro ao gravar ficheiro com resultados");
            }
        }
    }


    public static String[] choseFormatMenu() {
        Scanner in = new Scanner(System.in);
        System.out.println("Deseja gravar o gráfico em que formato?");
        System.out.println("1. txt");
        System.out.println("2. png");
        System.out.println("3. eps");
        System.out.println("Qualquer outra tecla para avançar");
        System.out.print("opção> ");

        int graphFormat = in.nextInt();
        switch (graphFormat) {
            case 1:
                return new String[]{"dumb", "txt"};
            case 2:
                return new String[]{"pngcairo", "png"};

            case 3:
                return new String[]{"postscript", "eps"};
        }
        return new String[0];
    }

    public static String chooseDirectoryMenu(String runningPath) {
        Scanner in = new Scanner(System.in);
        String answer;
        System.out.println("Deseja gravar o gráfico? [sim(default) ou não]");
        answer = in.nextLine().toLowerCase();
        if (answer.equals("") || answer.equals("sim")) {
            System.out.println();
            System.out.println("Em que directoria?");
            System.out.print("directoria> ");

            Scanner dir = new Scanner(System.in);
            if(!dir.nextLine().equals("")) {
                String directory = dir.nextLine();
                System.out.println(directory);
                return directory;
            }
        }
        return "";
    }
}

package com.grupo09;

import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.matrix.dense.Basic2DMatrix;

public class Algebra {

    /**
     * Method to calcute classes
     * {@code File}. Input matrix and vector from Utils.buildMatrix and Utils.buildVector method.??
     *
     * @implNote run both input's and multiply for each other, in correct position.
     *
     * @return the vector with the classes
     */
    public static double[] calculateClasses(double[][] matrix, double[] vector) {
        double[] response = new double[matrix[0].length];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                response[i] += matrix[i][j] * vector[j];
            }
        }
        return response;
    }

    /**
     * Method to sum all classes
     * {@code File}. Input vector from Utils.buildVector method.
     *
     * @implNote run the array and sum
     *
     * @return the total population
     */
    public static double totalPopulation(double[] vector) {
        double sum = 0;
        for (int i = 0; i < vector.length; i++)
            sum += vector[i];
        return sum;
    }

    /**
     * {@code File}. Input years and totalPopulation .
     *
     * @implNote run each year and the total population.
     *
     * @return total population per year.
     */
    public static double[] totalPopulationPerYear(int years, double[][] populationMatrix) {
        double[] totalPopulationPerYear = new double[years];
        for(int i = 0; i< years; i++) {
            totalPopulationPerYear[i] = Algebra.totalPopulation(populationMatrix[i]);
        }
        return totalPopulationPerYear;
    }

    /**
     * Method to calculate total poplation per year
     * {@code File}. Input years , matrix and vector from Utils.buildMatrix and Utils.buildVector method.
     *
     * @implNote use the calculateClasses method to print the total each year
     *
     * @return total population per class per year
     */
    public static double[][] totalPopulationPerClassPerYear(int years, double[][] matrix, double[] vector) {
        double[][] result = new double[years][vector.length];

        result[0] = vector;
        for(int i = 1; i < years; i++) {
            for(int j = 0; j < matrix[0].length; j ++) {
                result[i] = calculateClasses(matrix, result[i-1]);
            }
        }
        return result;
    }

    public static double[] populationGrowth(int years, double[][] populationMatrix) {
        double[] result = new double[years];
        for (int i = 1; i < years; i++) {
            result[i -1] = Algebra.totalPopulation(populationMatrix[i]) / Algebra.totalPopulation(populationMatrix[i - 1]);
        }
        return result;
    }

    /**
     * Method to calculate class normalized
     * {@code File}. Input population per class and total population from menu
     *
     * @implNote check class position, multiply for the total population
     *
     * @return calculate the population class normalized
     */
    public static double[][] populationPerClassNormalized(double[][] populationMatrix, double[] totalPopulationPerYear) {

        double[][] result = new double[populationMatrix.length][populationMatrix[0].length];

        for(int i = 0; i < populationMatrix.length; i++) {
            for(int j = 0; j < populationMatrix[i].length; j++) {
                result[i][j] = (populationMatrix[i][j] / totalPopulationPerYear[i]) * 100;
            }
        }
        return result;
    }

    /**
     * Method
     * {@code File}.
     *
     * @implNote
     * @return
     */
    public static double[] calcMaxEigenV(double[][] inputMatrix) {

        Matrix[] eigenMat = getEigenMatrix(inputMatrix);
        double[][] matA = eigenMat[0].toDenseMatrix().toArray();
        double[][] matB = eigenMat[1].toDenseMatrix().toArray();

        double[] eigenValues = getEigenValues(matB);
        int pos = findMaxEigenNumberPosition(eigenValues);

        double[] eigenVector = new double[matA[0].length];
        for(int j = 0; j < matA[0].length; j++) {
            eigenVector[j] = Math.abs(matA[j][pos]);
        }

        return eigenVector;
    }

    /**
     * Method to calculate de maximum eigenvalue
     * {@code File}. Input Leslie Matrix
     *
     * @implNote It gets the matrix with the eigenvalues calling "getEigenMatrix" method,
     * converts it to a dense Matrix and gets the Eigenvalues with "getEigenValues" method.
     *
     * @return The maximum Eigenvalue of the Matrix calling "findMaxEigenNumber" method
     */
    public static double calcMaxEigenValue(double[][] inputMatrix) {
        Matrix[] eigenMat = getEigenMatrix(inputMatrix);
        double[][] matB = eigenMat[1].toDenseMatrix().toArray();

        double[] eigenValues = getEigenValues(matB);
        return findMaxEigenNumber(eigenValues);
    }

    /**
     * {@code File}. Input file with the sample data.
     *
     * @implNote It gets the Eigenvalues Matrix
     *
     * @return The vector containing the population numbers, or
     *          empty vector if no valid or no values.
     */
    public static double[] calcMaxEigenVector(double[][] inputMatrix) {
        Matrix[] eigenMat = getEigenMatrix(inputMatrix);
        double[][] matB = eigenMat[1].toDenseMatrix().toArray();

        return getEigenValues(matB);
    }

    /**
     * {@code File}. Input Eigenvalues Matrix.
     *
     * @implNote It gets all the Eigenvalues of the matrix,
     * traversing the entire main diagonal
     *
     * @return An array with all the eigenvalues of the matrix
     */
    private static double[] getEigenValues(double[][] matB) {
        double[] eigenValues = new double[matB[0].length];
        for(int i = 0; i < matB[0].length; i++) {
            eigenValues[i] = matB[i][i];
        }
        return eigenValues;
    }

    /**
     * {@code File}. Input Leslie Matrix.
     *
     * @implNote It decomposes the Leslie Matrix into two separated matrices,
     * one with the eigenvectors, and the second one with the eigenvalues
     * of the main matrix.
     * @return Returns both the Eigenvectors Matrix, and the Eigenvalues matrix
     * in the same variable.
     */
    private static Matrix[] getEigenMatrix(double[][] inputMatrix) {
        Matrix matrix = new Basic2DMatrix(inputMatrix);
        EigenDecompositor eigenDecompositor = new EigenDecompositor(matrix);
        Matrix[] eigenMat = eigenDecompositor.decompose();
        return eigenMat;
    }

    /**
     * {@code File}. Input the Eigenvalues array.
     *
     * @implNote It runs through the array with all the eigenvalues,
     * taking the position 0 to comparate it with the other positions,
     * and see if the value in the next position is larger.
     *
     * @return The position of the maximum Eigenvalue
     */
    private static int findMaxEigenNumberPosition(double[] eigenValues) {
        int pos = 0;
        double max = eigenValues[0];

        for(int i = 0; i < eigenValues.length - 1; i++) {
            if(max < eigenValues[i+1]) {
                pos = i;
                max = eigenValues[i];
            }
        }
        return pos;
    }

    /**
     * {@code File}. Input Eigenvalues array.
     *
     * @implNote Runs almost in the same way as the previous method,
     * but comparing the value in the first position with the next ones
     * and see which one is larger
     *
     * @return The value of the maximum Eigenvalue.
     */
    private static double findMaxEigenNumber(double[] eigenValues) {
        double max = eigenValues[0];

        for(int i = 0; i < eigenValues.length - 1; i++) {
            if(max < eigenValues[i+1]) {
                max = eigenValues[i];
            }
        }
        return max;
    }

}

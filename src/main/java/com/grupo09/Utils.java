package com.grupo09;

import java.io.*;
import java.net.URISyntaxException;
import java.util.Scanner;

public class Utils {


    public static String[] getClassNames(File file) {
        String[] classNames = new String[0];
        try{
            int size = calculateSize(file);
            classNames = new String[size];
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if(line.contains("x")) {
                    String[] aux = line.split(", ");
                    for(int i = 0; i<aux.length; i++){
                        if(i <= 99){
                            classNames[i] = aux[i].substring(0,3);
                        }
                        if(i>=100) {
                            classNames[i] = aux[i].substring(0,4);
                        }
                    }
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("Ficheiro não encontrado");
        }
        return classNames;
    }

    /**
     * Method to build the matrix
     * {@code File}. Input file with the sample data.
     *
     * @implNote It calculates the size of the matrix using the calculateSize
     *             method. Then check if file exist and reads it. If the read line contains an
     *             "f" and "s" it will store all the numbers after the "=" in the matrix.
     *
     *
     * @return The matrix containing the survival rate and average of reproductive individuals, or
     *          empty matrix if no valid or no values.
     */

    public static double[][] buildMatrix(File file) {
        double[][] matrix = new double[0][];
        try{
            int size = calculateSize(file);
            matrix = new double[size][size];
            if(file.exists() && !file.isDirectory()) {
                Scanner scanner = new Scanner(file);
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    if (line.contains("f")) {
                        String[] fStrings = line.split(",");
                        for (int i = 0; i < fStrings.length; i++) {
                            String[] splitted = fStrings[i].split("=");
                            matrix[0][i] = Double.parseDouble(splitted[1]);
                        }
                    }
                    if (line.contains("s")) {
                        String[] sStrings = line.split(",");
                        for (int j = 0; j < sStrings.length; j++) {
                            String[] splitted = sStrings[j].split("=");
                            matrix[j + 1][j] = Double.parseDouble(splitted[1]);
                        }
                    }
                }
                scanner.close();
            }
        } catch (FileNotFoundException e) {
            System.out.println("Ficheiro não encontrado");
        }
        return matrix;
    }

    /**
     * Method to build a manual matrix
     * {@code}. Input f and s array created in generateVector method .
     *
     * @implNote use f size, because is the correct size. First place we put the matrix de 0.
     *           After that we put the numbers in correct place.
     *
     * @return a build matrix.
     */
    public static double[][] buildManualMatrix(double[] f, double[] s) {
        int size = f.length;
        double[][] matrix = new double[size][size];

        for(int i = 0; i< size; i++) {
            for(int j = 0; j < size; j++) {
                matrix[i][j] = 0;
            }
        }

        for(int ii = 0; ii < size; ii++) {
            matrix[0][ii] = f[ii];
        }

        for (int jj = 0; jj < size - 1; jj++) {
            matrix[jj + 1][jj] = s[jj];
        }

        return matrix;
    }

    /**
     * Method to build the vector
     * {@code File}. Input file with the sample data.
     *
     * @implNote It calculates the size of the vector using the calculateSize
     *             method. Then it reads the file and if the read line contains an
     *             "x" it will store all the numbers after the "=" in the vector.
     *             Each vector positions represents the quantity of the Population Class
     *
     * @return The vector containing the population numbers, or
     *          empty vector if no valid or no values.
     */
    public static double[] buildVector(File file) {
        double[] vector = new double[0];
        try{
            int size = calculateSize(file);
            vector = new double[size];
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if(line.contains("x")) {
                    String[] strings = line.split(",");
                    for(int i = 0; i < strings.length; i++) {
                        String[] splitted = strings[i].split("=");
                        vector[i] = Double.parseDouble(splitted[1]);
                    }
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("Ficheiro não encontrado");
        }
        return vector;
    }

    /**
     * Method Insert vector
     * {@code File}. input created in generateVector method.
     *
     * @implNote receive the size from countCommas method. remove "," from input and transform String
     *          in a double.
     *
     * @return the vector in the right size?
     */
    public static double[] insertVector(String input) {
        int size = countCommas(input);
        double[] result = new double[size];
        String[] inputs = input.split(",");

        for(int i = 0; i<inputs.length; i++) {
            result[i] = Double.parseDouble(inputs[i]);
        }
        return result;
    }

    /**
     * Method to print matrix
     * {@code File}. Input file with the sample data.
     *
     * @implNote run the all input
     *
     * @return the matrix
     */
    public static void printMatrix(double[][] matrix, String header) {
        System.out.println(header);
        for (int i = 0; i < matrix.length; i++) {
            System.out.printf("(%d", i);
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.printf(", %.2f", matrix[i][j]);
            }
            System.out.print(")");
            System.out.println();
        }
    }

    public static void printLeslieMatrix(double[][] matrix) {
        System.out.println("Matriz de Leslie");
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.printf("%.2f ", matrix[i][j]);
            }
            System.out.println();
        }
    }

    public static void formatLeslieMatrixToFile(PrintWriter printWriter, double[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                printWriter.printf("%.2f ", matrix[i][j]);
            }
            printWriter.println();
        }
    }

    public static void formatMatrixToFile(PrintWriter printWriter, double[][] matrix, String header) {
        printWriter.println(header);
        for (int i = 0; i < matrix.length; i++) {
            printWriter.printf("(%d", i);
            for (int j = 0; j < matrix[i].length; j++) {
                printWriter.printf(", %.2f", matrix[i][j]);
            }
            printWriter.print(")");
            printWriter.println();
        }
    }

    /**
     * Method print the Vector
     * {@code File}. Input file with the sample data.
     *
     * @implNote run the all input
     *
     * @return the vector
     */
    public static void printVector(double[] vector, int years, String header) {
        System.out.println(header);
        for (int i = 0; i < years; i++) {
            System.out.printf("(%d, %.2f)", i, vector[i]);
            System.out.println();
        }

    }

    public static void formatVectorToFile(PrintWriter printWriter, double[] vector, int years, String header) {
        printWriter.println(header);
        for (int i = 0; i < years; i++) {
            printWriter.printf("(%d, %.2f)", i, vector[i]);
            printWriter.println();
        }
    }

    public static void printEigenVector(double[] vector) {
        System.out.print("vector proprio associado=(");
        for (int i = 0; i < vector.length -1; i++) {
            System.out.printf("%.2f, ",vector[i]);
        }
        System.out.printf("%.2f)", vector[vector.length-1]);
    }

    public static void formatEigenVectorToFile(PrintWriter printWriter, double[] vector) {
        printWriter.print("vector proprio associado=(");
        for (int i = 0; i < vector.length -1; i++) {
            printWriter.printf("%.2f, ",vector[i]);
        }
        printWriter.printf("%.2f)", vector[vector.length-1]);
    }

    /**
     * Method to calculate size
     * {@code File}. Input file with the sample data.
     *
     * @implNote search in line from "x" and if appear, use the countCommas method to check the size.
     *
     * @return the size from line
     */
    private static int calculateSize(File file) {
        int size = 0;
        Scanner scanner;
        try {
            scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.contains("x")) {
                    size = countCommas(line);
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("Ficheiro não encontrado");
        }
        return size;
    }

    /**
     * Method to Count the numbers
     * {@code File}. Input from insertVector and calculateSize method
     *
     * @implNote check the input for "," . if the appear sum +1.
     *
     * @return size from the line.
     */
    private static int countCommas(String line) {
        int size = 0;
        for (int i = 0; i < line.length(); i++) {
            if(line.charAt(i) == ',') size++;
        }
        size += 1;
        return size;
    }

    /**
     * Method to clear the Screen
     * {@code File}. Input file with the sample data.
     *
     * @implNote remove and save back output????
     *
     * @return clear terminal
     */
    public static void clearScreen() {
        System.out.println("\033[H\033[2J");
        System.out.flush();
    }

    /**
     * Method to build the vector
     * {@code File}. Input vector and location path where the file is going to be saved.
     *
     * @implNote Writes vector values to a file that is going to be generated to
     * create the graphic using the values from the mentioned file.
     *
     */
    public static void writeVectorToFile(double[] vector, String path) {
        try {
            FileWriter fileWriter = new FileWriter(path);
            PrintWriter printWriter = new PrintWriter(fileWriter);

            for (int i = 0; i< vector.length; i++) {
                printWriter.println(i + " " + vector[i]);
            }
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeMatrixToFile(double[][] matrix, String path) {
        try {
            FileWriter fileWriter = new FileWriter(path);
            PrintWriter printWriter = new PrintWriter(fileWriter);

            for(int i = 0; i< matrix.length; i++) {
                printWriter.print(i + " ");
                for(int j = 0; j < matrix[i].length; j++){
                    printWriter.print(matrix[i][j] + " ");
                }
                printWriter.print("\n");
            }
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String buildHeader(double[][] matrix) {
        String result = ("(t, ");
        for(int i = 0; i < matrix.length - 1; i++) {
           result =  result + ("x" + (i+1) + ", ");
        }
        result = result + "x" + matrix.length + ")";
        return result;
    }

    public static void createTempFolder(String path) {
        File folder = new File(path);
        if (!folder.exists()){
            folder.mkdirs();
        }
    }

    public static String getRunningPath() throws URISyntaxException {
        String aux = Main.class
                .getProtectionDomain()
                .getCodeSource()
                .getLocation()
                .toURI()
                .getPath();

        return aux.replace(aux.substring(aux.lastIndexOf("/") + 1), "");
    }
}


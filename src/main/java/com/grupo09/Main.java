package com.grupo09;


import java.io.File;
import java.net.URISyntaxException;

public class Main {

    public static void main(String[] args) throws URISyntaxException {

        String runningPath = Utils.getRunningPath();

        Menu menu = new Menu();

        if(args.length == 0) {
            MenuHelper.interactive(menu, null, runningPath);
        }
        else if(args.length == 1) {
            String filename = args[0];
            File file = new File(filename);
            MenuHelper.interactive(menu, file, runningPath);
        }
        else {
            if(args.length > 1) {
                MenuHelper.nonInteractive(args);
            }
        }
    }

}

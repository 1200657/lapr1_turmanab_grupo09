package com.grupo09;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Graph {

    /**
     * {@code File}. Input the command to plot graphics from the command line.
     *
     * @implNote Executes gnuplot to show the graphic using the arguments from
     * the command line, and shows an error message if the execution fails.
     *
     * @return Error if the execution failed or finally executes if everything is ok.
     */
    public static void execGraph(String labelx, String labely, String[] classNames) {
        String[] cmd = showCmdBuilder(labelx, labely, classNames);
        try {
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(cmd);
            int execResult = proc.waitFor();
            if(execResult != 0) {
                System.out.println("Erro no gnuplot ao gerar o gráfico");
            }
            proc.getInputStream().close();
            proc.getOutputStream().close();
            proc.getErrorStream().close();
        } catch (Exception e) {
            System.err.println("Erro de runtime no processo de gerar gráfico");
        }
    }

    /**
     * {@code File}. Input file with the sample data, graphic, directory location
     * path, and desired file extension.
     *
     * @implNote Saves the graphic in the selected location path, formatting its name
     * with the desired extension calling the method "FormatSaveGraph". Throws an error message if the execution fails
     * when saving the graphic.
     *
     */
    public static void saveGraph(String populationName, String labelx, String labely, String[] classNames, String[] extension, String request) {
        String[] cmd = formatSaveGraph(populationName, labelx, labely, classNames, extension, request);
        try {
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(cmd);
            int execResult = proc.waitFor();
            if(execResult != 0) {
                System.out.println("Erro no gnuplot ao gravar o gráfico");
            }
            proc.getInputStream().close();
            proc.getOutputStream().close();
            proc.getErrorStream().close();
        } catch (Exception e) {
            System.err.println("Erro de runtime no processo de gravar gráfico");
        }

    }

    /**
     * {@code File}. Input file with the sample data, graphic, location path and desired extension.
     *
     * @implNote It applies a simples date format and the format required by the user in order to
     * select the location path where the graphic is going to be saved, and the desired extension
     * to save the output file.
     *
     * @return The command to save the output file with the required format.
     */
    private static String[] formatSaveGraph(String populationName, String labelx, String labely, String[] classNames, String[] extension, String request) {
        SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
        String date = format.format( new Date());
        String outputFilename =  "temp/" + populationName + request + date + "." + extension[1];
        String[] saveGraph = saveCmdBuilder(labelx, labely, classNames, extension, outputFilename);
        return saveGraph;
    }

    /**
     * {@code File}. Input labels for edges x and y, and the title for the graphic.
     *
     * @implNote Sets the labels for the x and y edges to show in the command line
     * and  also sets the title for the graphic
     *
     * @return The command with all the labels, title and styling for the graphic.
     */

    private static String[] showCmdBuilder(String labelx, String labely, String[] classNames) {
        String[] cmd = new String[]{"gnuplot", "-e", ""};
        cmd[2] = "set terminal qt;set xlabel \"" + labelx + "\";set ylabel \"" + labely + "\";";
        if(classNames.length == 1) {
            for (int i = 0; i < classNames.length; i++) {
                cmd[2] = cmd[2] + " plot \"temp/temp.txt\" using 1:" + (i + 2) + " with linespoints title \"" + classNames[i] + "\";";
            }
        }
        else{
            cmd[2] = cmd[2] + " plot";
            for (int i = 0; i < classNames.length - 1; i++) {
                cmd[2] = cmd[2] + " \"temp/temp.txt\" using 1:" + (i + 2) + " with linespoints title \"" + classNames[i] + "\",";
            }
            cmd[2] = cmd[2] + " \"temp/temp.txt\" using 1:" + (classNames.length + 2) + " with linespoints title \"" + classNames[classNames.length - 1] + "\";";
        }
        cmd[2] = cmd[2] + "pause -1;";
        return cmd;
    }

    /**
     * {@code File}. Input labels, title for the graphic, file extension and the output file name.
     *
     * @implNote Sets the labels for the x and y edges,
     * also sets the title for the graphic, and the output file extension with the name to save it
     *
     * @return The command to save the output graphic.
     */
    private static String[] saveCmdBuilder(String labelx, String labely, String[] classNames, String[] extension, String outputFilename) {
        String[] cmd = new String[]{"gnuplot", "-e", ""};
        cmd[2] = "set terminal " + extension[0] + ";set xlabel \"" + labelx + "\";set ylabel \"" + labely + "\"; set output \"" + outputFilename + "\";";
        if(classNames.length == 1) {
            for (int i = 0; i < classNames.length; i++) {
                cmd[2] = cmd[2] + " plot \"temp/temp.txt\" using 1:" + (i + 2) + " with linespoints title \"" + classNames[i] + "\";";
            }
        }
        else{
            cmd[2] = cmd[2] + " plot";
            for (int i = 0; i < classNames.length - 1; i++) {
                cmd[2] = cmd[2] + " \"temp/temp.txt\" using 1:" + (i + 2) + " with linespoints title \"" + classNames[i] + "\",";
            }
            cmd[2] = cmd[2] + " \"temp/temp.txt\" using 1:" + (classNames.length + 2) + " with linespoints title \"" + classNames[classNames.length - 1] + "\";";
        }
        return cmd;
    }

}

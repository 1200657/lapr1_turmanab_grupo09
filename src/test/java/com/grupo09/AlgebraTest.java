package com.grupo09;

import java.io.File;

public class AlgebraTest {

    private static final String FILENAME = "src/test/resources/bambis.txt";

    public static void main(String[] args) {
        runTests();
    }

    public static void runTests() {
        System.out.println("totalPopulation: " + (test_totalPopulation() ? "ok" : "nok"));
        System.out.println("totalPopulationPerYear: " + (test_totalPopulationPerYear() ? "ok" : "nok"));
        System.out.println("calculateClasses: " + (test_calculateClasses() ? "ok" : "nok"));
        System.out.println("populationPerClassNormalized: " + (test_populationPerClassNormalized() ? "ok" : "nok"));
        System.out.println("calcMaxEigenValue: " + (test_calcMaxEigenValue() ? "ok" : "nok"));
        System.out.println("calcMaxEigenVector: " + (test_calcMaxEigenVector() ? "ok" : "nok"));
    }

    private static boolean test_totalPopulation(){
        double[] vector = Utils.buildVector(new File(FILENAME));
        double expectedTotalPopulation = 1982.0;
        double actualTotalPopulation = Algebra.totalPopulation(vector);

        return expectedTotalPopulation == actualTotalPopulation;
    }

    private static boolean test_totalPopulationPerYear(){
        double[] vector = Utils.buildVector(new File(FILENAME));
        double[][] matrix = Utils.buildMatrix(new File(FILENAME));
        double[][] expectedTotalPopulation = {{566.0, 789.0, 400.0, 227.0},
                                              {4245.83, 113.2, 315.6, 160.0},
                                              {1307.1360000000002, 849.166, 45.28, 126.24000000000001}};
        double[][] actualTotalPopulationPerYear = Algebra.totalPopulationPerClassPerYear(3, matrix, vector);

        for(int i = 0; i < expectedTotalPopulation.length; i++) {
            for (int j = 0; i < actualTotalPopulationPerYear.length; i++){
                if(!(expectedTotalPopulation[i][j] == actualTotalPopulationPerYear[i][j])) {
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean test_calculateClasses(){
        double[] vector = Utils.buildVector(new File(FILENAME));
        double[][] matrix = Utils.buildMatrix(new File(FILENAME));
        double[] expected = {4245.83, 113.2, 315.6, 160};
        double[] actual = Algebra.calculateClasses(matrix, vector);

        if(expected.length!=actual.length){
            return false;
        }
        for(int i = 0; i < expected.length; i++) {
            if(expected[i]!=actual[i]) {
                return false;
            }
        }
        return true;
    }

    private static boolean test_populationPerClassNormalized() {
        double[] vector = Utils.buildVector(new File(FILENAME));
        double[][] matrix = Utils.buildMatrix(new File(FILENAME));
        double[][] expectedMatrix = {{0.0, 0.7067137809187279, 0.45229681978798586, 0.05123674911660776},
                                     {0.02534854245880862, 0.0, 0.0, 0.0},
                                     {0.0, 0.1, 0.0, 0.0},
                                     {0.0, 0.0, 0.1762114537444934, 0.0}};

        double[][] actual = Algebra.populationPerClassNormalized(matrix, vector);

        if(expectedMatrix.length!=actual.length){
            return false;
        }
        for(int i = 0; i < expectedMatrix.length; i++){
            for (int j = 0; j < actual.length; j++){
                if(expectedMatrix[i][j] != actual[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean test_calcMaxEigenValue(){
        double[] vector = Utils.buildVector(new File(FILENAME));
        double[][] matrix = Utils.buildMatrix(new File(FILENAME));
        double expected = 1.0063189494046383;
        double actual = Algebra.calcMaxEigenValue(matrix);

        return expected == actual;
    }

    private static boolean test_calcMaxEigenVector(){
        double[] vector = Utils.buildVector(new File(FILENAME));
        double[][] matrix = Utils.buildMatrix(new File(FILENAME));
        double[] expected = {1.0063189494046383,-0.7335148550404906,-0.2140779525619012,-0.058726141802246105};
        double[] actual = Algebra.calcMaxEigenVector(matrix);

        if(expected.length != actual.length){
            return false;
        }
        for(int i = 0;i < expected.length;i++)
        {
            if(expected[i]!=actual[i]) {
                return false;
            }
        }
        return true;
    }

}


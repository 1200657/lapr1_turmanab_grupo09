package com.grupo09;


import java.io.File;

public class UtilsTest {

    private static final String FILENAME = "src/test/resources/input.txt";

    public static void main(String[] args) {
        runTests();
    }

    public static void runTests() {

        System.out.println("buildMatrix: " + (test_buildMatrix() ? "ok" : "nok"));
        System.out.println("buildVector: " + (test_buildVector() ? "ok" : "nok"));
        System.out.println("insertVector: " + (test_insertVector() ? "ok" : "nok"));
        System.out.println("buildManualMatrix: " + (test_buildManualMatrix() ? "ok" : "nok"));
    }

    private static boolean test_buildMatrix(){
        File file = new File(FILENAME);

        double[][] expectedMatrix = {
                {0.50, 2.40, 1.00, 0.00},
                {0.50, 0.00, 0.00, 0.00},
                {0.00, 0.80, 0.00, 0.00},
                {0.00, 0.00, 0.50, 0.00}};

        double[][] actualMatrix = Utils.buildMatrix(file);

        for(int i = 0; i < expectedMatrix.length; i++) {
            if(expectedMatrix[i].length != actualMatrix[i].length) {
                return false;
            }
            for(int j = 0; j < expectedMatrix.length; j++) {
                if(expectedMatrix[i][j] != actualMatrix[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean test_buildVector() {
        File file = new File(FILENAME);

        double[] expectedVector = {20, 10, 40, 30};
        double[] actualVector = Utils.buildVector(file);

        for (int i = 0; i < expectedVector.length; i++) {
            if (expectedVector[i] != actualVector[i])
                return false;
        }
        return true;
    }

    private static boolean test_insertVector() {
        double[] expectedVector = {20, 10, 40, 30};
        String inputVector = "20,10,40,30";
        double[] actualVector = Utils.insertVector(inputVector);

        for (int i = 0; i < expectedVector.length; i++) {
            if (expectedVector[i] != actualVector[i])
                return false;

        }
        return true;
    }

    private static boolean test_buildManualMatrix() {
        double[][] expectedMatrix = {
                {0.50, 2.40, 1.00, 0.00},
                {0.50, 0.00, 0.00, 0.00},
                {0.00, 0.80, 0.00, 0.00},
                {0.00, 0.00, 0.50, 0.00}};

        double[] inputF = {0.50, 2.40, 1.00, 0.00};
        double[] inputS = {0.50, 0.80, 0.50};

        double[][] actualMatrix = Utils.buildManualMatrix(inputF, inputS);

        for(int i = 0; i < expectedMatrix.length; i++) {
            if(expectedMatrix[i].length != actualMatrix[i].length) {
                return false;
            }
            for(int j = 0; j < expectedMatrix.length; j++) {
                if(expectedMatrix[i][j] != actualMatrix[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }
}

